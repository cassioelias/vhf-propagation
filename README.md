Política de Privacidade do VHF Propagation

Última atualização: 09-12-2023

Bem-vindo ao VHF Propagation ("nós", "nosso" ou "aplicativo"). Esta página informa sobre nossas políticas relativas à coleta, uso e divulgação de informações pessoais quando você utiliza nosso serviço.

Informações que Coletamos
Dados Pessoais:

Podemos coletar informações pessoais que você fornece, como nome, endereço de e-mail, número de telefone, entre outros, quando você se cadastra ou usa nosso aplicativo.
Dados de Uso:

Coletamos informações sobre como você utiliza nosso aplicativo, como páginas visitadas, recursos utilizados e interações com o aplicativo.
Informações de Dispositivo:

Podemos coletar informações sobre o dispositivo que você está usando, incluindo o modelo, sistema operacional, identificadores exclusivos e informações de rede.
Como Utilizamos suas Informações
Utilizamos as informações coletadas para diversos propósitos, incluindo:

Fornecer e manter nosso serviço.
Personalizar e melhorar nosso aplicativo.
Analisar o uso do aplicativo para entender e melhorar a experiência do usuário.
Compartilhamento de Informações
Com terceiros:

Podemos compartilhar suas informações com terceiros apenas quando necessário para fornecer nosso serviço ou cumprir obrigações legais.
Consentimento:

Ao utilizar nosso aplicativo, você concorda com a coleta e uso de informações de acordo com esta política de privacidade.
Segurança
Tomamos medidas razoáveis para proteger suas informações contra acesso não autorizado ou alteração, mas lembre-se de que nenhum método de transmissão pela internet ou método de armazenamento eletrônico é 100% seguro.

Alterações nesta Política de Privacidade
Esta política de privacidade pode ser atualizada ocasionalmente. Aconselhamos que você reveja esta página periodicamente para estar ciente de quaisquer alterações. Informaremos sobre qualquer alteração publicando a nova política de privacidade nesta página.

Contato
Se você tiver alguma dúvida ou sugestão sobre nossa política de privacidade, entre em contato conosco em [seu e-mail de contato].

Ao utilizar nosso aplicativo, você concorda com esta política de privacidade.